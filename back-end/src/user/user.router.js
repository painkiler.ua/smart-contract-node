import { Router } from 'express'

import ctrl from './user.controller';
import { checkAuth } from '../helpers'

const router = new Router()

export default router
    .post('/', checkAuth, ctrl.call('create'))