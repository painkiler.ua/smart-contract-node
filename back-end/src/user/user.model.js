import mongoose, { Schema } from 'mongoose'
// import bluebird from 'bluebird'
// import bcrypt from 'bcrypt'

// const hash = bluebird.promisify(bcrypt.hash, { context: bcrypt })
// const compare = bluebird.promisify(bcrypt.compare, { context: bcrypt })

const userSchema = new Schema({
    login: {
        type: String,
        required: true,
        unique: true
    },
    email: {
        type: String,
        required: true,
        unique: true
    },
    firstName: {
      type: String,
    },
    lastName: {
      type: String,
    },
    avatarUri: {
        type: String,
    },
    password:{
        type: String,
    },
    roles: [String],
    isConfirmed: {
        type: Boolean
    }
}, {
timestamps: true
})

// userSchema.methods.checkPassword = function (password) {
//     return compare(password, this.password)
// }

// userSchema.statics.getHash = function (password) {
//     return hash(password, 10)
// }

export default mongoose.model('User', userSchema)