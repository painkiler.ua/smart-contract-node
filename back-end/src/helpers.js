import jwt from 'jsonwebtoken'
import config from './config'

/**
 * check auth
 * @param req
 * @param res
 * @param next
 */
export const checkAuth = (req, res, next) => {
    const token = req.headers['authorization'];

    if (token) {
      jwt.verify(token, config.jwt.secret, (err, user) => {
        if (err) {
          return res.status(401).json({
            message: 'Failed to authenticate token'
          })
        }

        next();
      })
    } else {
      return res.status(401).json({
        message: 'No token provided.'
      })
    }
  }