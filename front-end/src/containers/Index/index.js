import React, { Component, Fragment } from 'react';
import { connect } from 'react-redux';

import './index.scss';

class Index extends Component {
    state = {
    }
    componentDidMount () {
    }

    render () {
        const {  } = this.props;
        return (
            <Fragment>
                <div>Main</div>
            </Fragment>
        )
    }
}

const mapStateToProps = state => {
    return {
        users: []
    };
};

const mapDispatchToProps = dispatch => {
    return {
        fetchUsers: () => {}
    };
};

export default connect( mapStateToProps, mapDispatchToProps )(Index);