import React, { Component, Fragment } from 'react';
import { connect } from 'react-redux';

import './index.scss';

import * as actions from '../../store/actions'

import Input from '../../components/UI/Input'
class SignUp extends Component {
    state = {
        login: '',
        email: '',
        password: ''

    }

    onInputHandler = (e) => {
        const target = e.target
        const value = target.type === 'checkbox' ? target.checked : target.value
        const name = target.name;
        this.setState({
            [name]: value
        })
    }

    createAccount = (e) => {
        e.preventDefault()
        this.props.createAccount(this.state)
    }

    render () {
        const {  } = this.props;
        return (
            <div className="container">
                <div className="row">
                    <div className="col-md">
                    </div>
                    <div className="col-md">
                        <form className="my-5">
                            <span className="d-block my-2">Create your personal account</span>
                            <div className="form-group">
                                <Input
                                    label='Username'
                                    id='join-username'
                                    type='text'
                                    name='login'
                                    onChangeHandler={this.onInputHandler}
                                />
                            </div>
                            <div className="form-group">
                                <Input
                                    label='Email address'
                                    id='join-email'
                                    type='text'
                                    name='email'
                                    onChangeHandler={this.onInputHandler}
                                />
                                <small id="emailHelp" className="form-text text-muted">We'll never share your email with anyone else.</small>
                            </div>
                            <div className="form-group">
                                <Input
                                    label='Password'
                                    id='join-password'
                                    type='password'
                                    name='password'
                                    onChangeHandler={this.onInputHandler}
                                />
                            </div>
                            <button className="btn btn-primary" onClick={this.createAccount}>Create an account</button>
                        </form>
                    </div>
                    <div className="col-md">
                    </div>
                </div>
            </div>
        )
    }
}

const mapStateToProps = state => {
    return {
        users: []
    };
};

const mapDispatchToProps = dispatch => {
    return {
        createAccount: (payload) => dispatch(actions.createAccount(payload))
    };
};

export default connect( mapStateToProps, mapDispatchToProps )(SignUp);