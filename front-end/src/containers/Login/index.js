import React, { Component, Fragment } from 'react';
import { connect } from 'react-redux';

import './index.scss';

class Login extends Component {
    state = {
    }

    render () {
        const {  } = this.props;
        return (
            <div className="container">
                <div className="row">
                    <div className="col-md">
                    </div>
                    <div className="col-md">
                        <form className="my-5">
                            <div className="form-group">
                                <label htmlFor="exampleInputEmail1">Email address</label>
                                <input type="email" className="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter email" />
                                <small id="emailHelp" className="form-text text-muted">We'll never share your email with anyone else.</small>
                            </div>
                            <div className="form-group">
                                <label htmlFor="exampleInputPassword1">Password</label>
                                <input type="password" className="form-control" id="exampleInputPassword1" placeholder="Password" />
                            </div>
                            <div className="form-check">
                                <input type="checkbox" className="form-check-input" id="exampleCheck1" />
                                <label className="form-check-label" htmlFor="exampleCheck1">Check me out</label>
                            </div>
                            <button type="submit" className="btn btn-primary">Submit</button>
                        </form>
                    </div>
                    <div className="col-md">
                    </div>
                </div>
            </div>
        )
    }
}

const mapStateToProps = state => {
    return {
        users: []
    };
};

const mapDispatchToProps = dispatch => {
    return {
        fetchUsers: () => {}
    };
};

export default connect( mapStateToProps, mapDispatchToProps )(Login);