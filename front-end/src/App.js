import React, { Component, Fragment } from 'react'
import { Route, Switch } from 'react-router-dom'

import Index from './containers/Index'
import SignUp from './containers/SignUp'
import Login from './containers/Login'
import Header from './components/Header'
import Footer from './components/Footer'

class App extends Component {
  state = {}
  render() {
    return (
      <Fragment>
        <Header />
          <main role="main">
            <Switch>
              <Route path="/" exact component={Index} />
              <Route path="/login" exact component={Login} />
              <Route path="/join" exact component={SignUp} />
            </Switch>
          </main>
        <Footer />
      </Fragment>
    );
  }
}

export default App;
