import React from 'react'
import { Link } from 'react-router-dom'

import './index.scss'
import logo from '../../assets/img/black-sagewise-logo-with-transparency.png'

const Header = props => (
    <header className="navbar sticky-top">
        <div className="container">
            <div className="d-flex justify-content-end w-100">
                <Link to={'/login'}>Sign in</Link>
                        <span className="mx-2">or</span>
                <Link to={'/join'}>Sign up</Link>   
            </div>
        </div>
    </header>
)

export default Header