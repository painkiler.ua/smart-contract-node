import React, { Fragment } from 'react'

const Input = props => {
    const {
        label,
        name,
        id,
        placeholder,
        type,
        onChangeHandler
    } = props
    return (
        <Fragment>
            <label htmlFor={id}>{label}</label>
            <input
                type={type}
                className="form-control"
                id={id}
                name={name}
                placeholder={placeholder}
                onChange={onChangeHandler}
            />
        </Fragment>
    )
}

export default Input