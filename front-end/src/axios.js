import axios from 'axios';

let baseURL;
if (process.env.NODE_ENV === 'production') {
    // baseURL = 'http://34.207.65.88/api';
} else {
    // baseURL = 'http://localhost:8080/api';
}

const instance = axios.create({
    baseURL
});

export default instance;