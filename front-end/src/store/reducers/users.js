import {
    CREATE_ACCOUNT_START,
    CREATE_ACCOUNT_SUCCESS,
    CREATE_ACCOUNT_FAIL,
} from '../actions/actionTypes';


const initialState = {
    users: [],
    loading: false,
    hasError: false
};

const reducer = ( state = initialState, action ) => {
    switch ( action.type ) {
        case CREATE_ACCOUNT_START: 
            return {
                ...state,
                loading: true,
                hasError: true
            }
        case CREATE_ACCOUNT_SUCCESS: 
            return {
                ...state,
            }
        case CREATE_ACCOUNT_FAIL: 
            return {
                ...state,
                loading: false,
                hasError: true
            }
        default: return state;
    }
};

export default reducer;