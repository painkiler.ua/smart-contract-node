import {
    CREATE_ACCOUNT_START,
    CREATE_ACCOUNT_SUCCESS,
    CREATE_ACCOUNT_FAIL,
} from './actionTypes';

import axios from '../../axios';

export const createAccountStart = () => ({
    type: CREATE_ACCOUNT_START
})

export const createAccountSuccess = () => ({
    type: CREATE_ACCOUNT_SUCCESS
})

export const createAccountFail = () => ({
    type: CREATE_ACCOUNT_FAIL 
})

export const createAccount = (payload) => (
    dispatch => {
        dispatch(createAccountStart());

        axios({
            method: 'post',
            url: '/users',
            data: payload,
        }).then( res => {
            dispatch(createAccountSuccess());
        }).catch( err => {
            console.log('ERRORS', err)
            dispatch(createAccountFail())
        })
    }
)